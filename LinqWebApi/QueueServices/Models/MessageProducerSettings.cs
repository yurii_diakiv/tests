﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;

namespace QueueServices.Models
{
    public class MessageProducerSettings
    {
        public IModel Channel { get; set; }
        public PublicationAddress PublicationAddress { get; set; }
    }
}
