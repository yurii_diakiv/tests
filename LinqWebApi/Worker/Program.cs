﻿using System;
using QueueServices.Services;
using QueueServices.Models;
using Microsoft.Extensions.DependencyInjection;

namespace Worker
{
    
    class Program
    {
        static void Main(string[] args)
        {
            ConnectionFactory connectionFactory = new ConnectionFactory(new Uri("amqp://guest:guest@localhost:5672"));

            MessageProducerScopeFactory messageProducerScopeFactory = new MessageProducerScopeFactory(connectionFactory);
            MessageConsumerScopeFactory messageConsumerScopeFactory = new MessageConsumerScopeFactory(connectionFactory);

            MessageService messageService = new MessageService(messageConsumerScopeFactory, messageProducerScopeFactory);
            messageService.Run();
        }
    }
}
