﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class UsersService : IUsersService
    {
        private readonly IRepository<User> repository;

        public UsersService(UserRepository userRepository)
        {
            repository = userRepository;
        }
        public async Task<IEnumerable<User>> GetUsers()
        {
            return await repository.GetItems();
        }

        public async Task<User> GetUser(int id)
        {
            return await repository.GetItem(id);
        }

        public async Task Create(User item)
        {
            await repository.Create(item);
        }

        public async Task Update(User user)
        {
            await repository.Update(user);
        }

        public async Task Delete(int id)
        {
            await repository.Delete(id);
        }
    }
}
