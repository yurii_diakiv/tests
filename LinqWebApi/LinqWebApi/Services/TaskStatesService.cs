﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TaskStatesService : ITaskStatesService
    {
        private readonly IRepository<TaskState> repository;

        public TaskStatesService(TaskStateRepository taskStateRepository)
        {
            repository = taskStateRepository;
        }
        public async Task<IEnumerable<TaskState>> GetTaskStates()
        {
            return await repository.GetItems();
        }

        public async Task<TaskState> GetTaskState(int id)
        {
            return await repository.GetItem(id);
        }

        public async Task Create(TaskState item)
        {
            await repository.Create(item);
        }

        public async Task Update(TaskState taskState)
        {
            await repository.Update(taskState);
        }

        public async Task Delete(int id)
        {
            await repository.Delete(id);
        }
    }
}
