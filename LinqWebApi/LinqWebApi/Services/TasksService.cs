﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TasksService : ITasksService
    {
        private readonly IRepository<ProjectTask> repository;

        public TasksService(TaskRepository taskRepository)
        {
            repository = taskRepository;
        }
        public async Task<IEnumerable<ProjectTask>> GetTasks()
        {
            return await repository.GetItems();
        }

        public async Task<ProjectTask> GetTask(int id)
        {
            return await repository.GetItem(id);
        }

        public async Task Create(ProjectTask item)
        {
            await repository.Create(item);
        }

        public async Task Update(ProjectTask task)
        {
            await repository.Update(task);
        }

        public async Task Delete(int id)
        {
            await repository.Delete(id);
        }
    }
}
