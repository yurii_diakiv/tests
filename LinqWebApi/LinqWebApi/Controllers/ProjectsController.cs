﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
//using LinqWebApi.Models;
using DataAccess.Entities;
using LinqWebApi.Repositories;
using Newtonsoft.Json;
using System.Net.Http;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectsService projectsService;

        public ProjectsController(ProjectsService service)
        {
            projectsService = service;
        }

        [HttpGet]
        public async Task<IEnumerable<Project>> Get()
        {
            return await projectsService.GetProjects();
        }

        [HttpGet("{id}")]
        public async Task<Project> Get(int id)
        {
            return await projectsService.GetProject(id);
        }

        [HttpPost]
        public async Task Post([FromBody] Project project)
        {
            await projectsService.Create(project);
        }

        [HttpPut]
        public async Task Put([FromBody] Project project)
        {
            await projectsService.Update(project);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await projectsService.Delete(id);
        }
    }
}
