﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStatesController : ControllerBase
    {
        private readonly TaskStatesService taskStatesService;

        public TaskStatesController(TaskStatesService service)
        {
            taskStatesService = service;
        }

        [HttpGet]
        public async Task<IEnumerable<TaskState>> Get()
        {
            return await taskStatesService.GetTaskStates();
        }

        [HttpGet("{id}")]
        public async Task<TaskState> Get(int id)
        {
            return await taskStatesService.GetTaskState(id);
        }

        [HttpPost]
        public async Task Post([FromBody] TaskState taskState)
        {
            await taskStatesService.Create(taskState);
        }

        [HttpPut]
        public async Task Put([FromBody] TaskState taskState)
        {
            await taskStatesService.Update(taskState);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await taskStatesService.Delete(id);
        }
    }
}
