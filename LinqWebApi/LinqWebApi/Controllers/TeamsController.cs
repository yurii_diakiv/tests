﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamsService teamsService;

        public TeamsController(TeamsService service)
        {
            teamsService = service;
        }

        [HttpGet]
        public async Task<IEnumerable<Team>> Get()
        {
            return await teamsService.GetTeams();
        }

        [HttpGet("{id}")]
        public async Task<Team> Get(int id)
        {
            return await teamsService.GetTeam(id);
        }

        [HttpPost]
        public async Task Post([FromBody] Team team)
        {
            await teamsService.Create(team);
        }

        [HttpPut]
        public async Task Put([FromBody] Team team)
        {
            await teamsService.Update(team);
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await teamsService.Delete(id);
        }
    }
}
