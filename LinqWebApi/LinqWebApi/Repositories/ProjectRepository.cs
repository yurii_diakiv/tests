﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ProjectDbContext dbContext;

        public ProjectRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public async Task<IEnumerable<Project>> GetItems()
        {
            return await dbContext.Set<Project>().ToListAsync();
        }

        public async Task<Project> GetItem(int id)
        {
            return await dbContext.Set<Project>().FindAsync(id);
        }

        public async Task Create(Project p)
        {
            await dbContext.Set<Project>().AddAsync(p);
            await dbContext.SaveChangesAsync();
        }

        public async Task Update(Project p)
        {
            dbContext.Entry<Project>(p).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var p = await dbContext.Set<Project>().FindAsync(id);
            dbContext.Set<Project>().Remove(p);
            await dbContext.SaveChangesAsync();
        }
    }
}
