﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ProjectDbContext dbContext;

        public TeamRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public async Task<IEnumerable<Team>> GetItems()
        {
            return await dbContext.Set<Team>().ToListAsync();
        }

        public async Task<Team> GetItem(int id)
        {
            return await dbContext.Set<Team>().FindAsync(id);
        }

        public async Task Create(Team p)
        {
            await dbContext.AddAsync(p);
            await dbContext.SaveChangesAsync();
        }

        public async Task Update(Team p)
        {
            dbContext.Entry<Team>(p).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var p = await dbContext.Set<Team>().FindAsync(id);
            dbContext.Set<Team>().Remove(p);
            await dbContext.SaveChangesAsync();
        }
    }
}
