﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ProjectDbContext dbContext;

        public UserRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public async Task<IEnumerable<User>> GetItems()
        {
            return await dbContext.Set<User>().ToListAsync();
        }

        public async Task<User> GetItem(int id)
        {
            return await dbContext.Set<User>().FindAsync(id);
        }

        public async Task Create(User p)
        {
            await dbContext.Set<User>().AddAsync(p);
            await dbContext.SaveChangesAsync();
        }

        public async Task Update(User p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var p = await dbContext.Set<User>().FindAsync(id);
            dbContext.Set<User>().Remove(p);
            await dbContext.SaveChangesAsync();
        }
    }
}
