﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class TaskRepository : IRepository<ProjectTask>
    {
        private readonly ProjectDbContext dbContext;

        public TaskRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public async Task<IEnumerable<ProjectTask>> GetItems()
        {
            return await dbContext.Set<ProjectTask>().ToListAsync();
        }

        public async Task<ProjectTask> GetItem(int id)
        {
            return await dbContext.Set<ProjectTask>().FindAsync(id);
        }

        public async Task Create(ProjectTask p)
        {
            await dbContext.Set<ProjectTask>().AddAsync(p);
            await dbContext.SaveChangesAsync();
        }

        public async Task Update(ProjectTask p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            await dbContext.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            var p = await dbContext.Set<ProjectTask>().FindAsync(id);
            dbContext.Set<ProjectTask>().Remove(p);
            await dbContext.SaveChangesAsync();
        }
    }
}
