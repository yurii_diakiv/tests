﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore.InMemory;
using Microsoft.EntityFrameworkCore;
using DataAccess;
using LinqWebApi.Repositories;
using DataAccess.Entities;
using System.Threading.Tasks;

namespace LinqWebApi.Tests.Repositories.IntegrationalTests
{
    [TestFixture]
    class TaskRepositoryInterationalTest
    {
        [Test]
        public async Task RemoveTaskTest()
        {
            var dbOptions = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase("RemoveTaskTest")
                .Options;

            var dbContext = new ProjectDbContext(dbOptions);
            var tRepository = new TaskRepository(dbContext);

            ProjectTask projectTask = new ProjectTask
                (
                    1,
                    "fggqq",
                    "asgazzz",
                    new DateTime(2014, 4, 1),
                    new DateTime(2015, 7, 6),
                    2,
                    3,
                    4
                );
            await tRepository.Create(projectTask);

            Assert.Contains(projectTask, (await tRepository.GetItems()).ToList());

            await tRepository.Delete(1);

            Assert.False((await tRepository.GetItems()).ToList().Contains(projectTask));
        }
    }
}