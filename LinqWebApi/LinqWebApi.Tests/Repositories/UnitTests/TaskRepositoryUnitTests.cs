﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using DataAccess.Entities;
using FakeItEasy;
using LinqWebApi.Repositories;

namespace LinqWebApi.Tests.Repositories.UnitTests
{
    [TestFixture]
    class TaskRepositoryUnitTests
    {
        [Test]
        public void ChangeTaskStateTest()
        {
            var fakeTaskRepository = A.Fake<IRepository<ProjectTask>>();

            ProjectTask projectTask = new ProjectTask
            (
                666,
                "jrmxkw",
                "wqfqfccc",
                new DateTime(2019, 2, 2),
                new DateTime(2019, 3, 3),
                1,
                3,
                2
            );

            fakeTaskRepository.Create(projectTask);

            A.CallTo(() => fakeTaskRepository.Create(A<ProjectTask>._)).MustHaveHappenedOnceExactly();

            ProjectTask projectTask2 = new ProjectTask
                (
                    666,
                    "jrmxkw",
                    "wqfqfccc",
                    new DateTime(2019, 2, 2),
                    new DateTime(2019, 3, 3),
                    3,
                    3,
                    2
                );

            fakeTaskRepository.Update(projectTask2);

            A.CallTo(() => fakeTaskRepository.Update(A<ProjectTask>._)).MustHaveHappenedOnceExactly();
        }
    }
}
