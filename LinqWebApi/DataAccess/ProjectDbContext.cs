﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DataAccess.Entities;

namespace DataAccess
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            :base(options) { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<ProjectTask> ProjectTasks { get; set; }
        public DbSet<TaskState> TaskStates { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DataAccess.Data data = new DataAccess.Data();
            List<Project> projects = data.GetProjects();
            List<ProjectTask> projectTasks = data.GetTasks();
            List<TaskState> taskStates = data.GetTaskStates();
            List<Team> teams = data.GetTeams();
            List<User> users = data.GetUsers();

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<ProjectTask>().HasData(projectTasks);
            modelBuilder.Entity<TaskState>().HasData(taskStates);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
        }
    }
}
